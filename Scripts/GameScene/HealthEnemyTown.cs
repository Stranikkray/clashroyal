﻿using UnityEngine;
using UnityEngine.UI;

public class HealthEnemyTown : MonoBehaviour
{
    public int Health = 100;
    public GameObject Enemytown;
    public Text textHealthEnemyTown;

    // Update is called once per frame
    void Update()
    {
        textHealthEnemyTown.text = "Health Enemy Town = " + Health.ToString();

        if (Health <= 0)
        {
            Destroy(Enemytown);
        }      
    }
}
