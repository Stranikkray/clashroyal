﻿using UnityEngine;

public class MoneyMakings : MonoBehaviour
{
    public GameObject MoneyManeg;
    public float spawnTime = 5f;
    public int moneyMakings;

    void Start()
    {
        if (MoneyManeg != null)
        {
            InvokeRepeating("SpawnMoney", spawnTime, spawnTime);
        }      
    }

    private void OnTriggerEnter(Collider other)
    {
        if (MoneyManeg == null)
        {
            if (other.gameObject.tag == "MoneyManeg")
            {               
                MoneyManeg = other.gameObject;
                InvokeRepeating("SpawnMoney", spawnTime, spawnTime);
            }            
        }
    }


    public void SpawnMoney()
    {
        MoneyManeg.GetComponent<MoneyManeg>().money += moneyMakings;
    }
}
