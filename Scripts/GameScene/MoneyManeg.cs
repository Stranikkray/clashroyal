﻿using UnityEngine;
using UnityEngine.UI;

public class MoneyManeg : MonoBehaviour
{
    public Text moneytext;
    public Text sellMoneyText;

    public int money;
    public bool Buying;
    public int sellMoney;


    void Start()
    {
        money = 100;
        Buying = false;
    }
    private void Update()
    {
        moneytext.text = " Money= " + money;
        sellMoneyText.text = " -" + sellMoney;

        if (Buying)
        {
            money -= sellMoney;
            Buying = false;
        }
       
    }
   

    public void House()
    {
        sellMoney = 50;
    }

    public void Tower()
    {
        sellMoney = 100;
    }

    public void Farm()
    {
        sellMoney = 70;
    }

}
