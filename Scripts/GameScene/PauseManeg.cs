﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManeg : MonoBehaviour
{
    public GameObject panel;

    public void Pause()
    {
        Time.timeScale = 0;
        panel.SetActive(true);
    }
    public void Сontinuation()
    {
        Time.timeScale = 1;
        panel.SetActive(false);
    }
    public void ExitToMenu()
    {
        SceneManager.LoadScene("Menu");
    } 

}
