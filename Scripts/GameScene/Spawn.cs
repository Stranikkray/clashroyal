﻿using UnityEngine;
using UnityEngine.UI;

public class Spawn : MonoBehaviour
{
    public GameObject hero;
    private float spawnTime = 1f;
    public Transform spawnPosition;
    public Vector3 NewSpawn;
    public Renderer MainRenderer;
    private int Shet;
    public Slider slider;

    void Start()
    {
        Shet = 0;
        InvokeRepeating("Spaw", spawnTime, spawnTime);
    }

    // Update is called once per frame
    public void Spaw()
    {
        if (MainRenderer.material.color == Color.white)
        {            
            Shet++;
            slider.value = Shet;

            if (Shet == 10)
            {
                Vector3 spawn = new Vector3(spawnPosition.position.x + NewSpawn.x, spawnPosition.position.y + NewSpawn.y, spawnPosition.position.z + NewSpawn.z);
                Instantiate(hero, spawn, spawnPosition.rotation);
                Shet = 0;
                slider.value = Shet;
            }
            Debug.Log(Shet);

        }
    }
}
