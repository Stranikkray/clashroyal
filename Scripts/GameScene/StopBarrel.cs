﻿using UnityEngine;
using UnityEngine.AI;
//using UnityEngine.AI;

public class StopBarrel : MonoBehaviour
{ 
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player") 
        {
            other.gameObject.GetComponent<NavMeshAgent>().speed = 0; 
        }
    }
}
